invlink = (content, ref) ->
  return "<a href='./_inv_#{ref}' class='once'>#{content}</a>"

class Character
  constructor: (spec) ->
    @inventory = []

    @take = (thing) =>
      @inventory.push thing
    @drop = (thing) =>
      for i in @inventory
        if i.name == thing
          index = @objects.indexOf(thing)
          @inventory.splice(index, 1)

    @has = (thing) =>
      for i in @inventory
        if i.name == thing
          return true
      return false

    @listinv = (thing) =>
      for i in @inventory
        if i.name == thing
          return invlink(i.name, i.name)

    @inv = (thing) =>
      for i in @inventory
        if i.name == thing
          return i.inv.fcall(i)

    for index, value of spec
      this[index] = value
    return this

character = (spec) ->
  spec ?= {}
  return( new Character(spec) )

module.exports = character

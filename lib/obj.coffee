markdown = require('./markdown.coffee')
require('./salet.coffee')
objlink = (content, ref) ->
  return "<a href='./_act_#{ref}' class='once'>#{content}</a>"

Array::remove = (e) -> @[t..t] = [] if (t = @indexOf(e)) > -1

parsedsc = (text, name) ->
  window.objname = name
  text = text.replace /\{\{(.+)\}\}/g, (str, p1) ->
    name = window.objname
    window.objname = undefined
    return objlink(p1, name)
  return text

# An object class.
# An object cannot be in several locations at once, you must clone the variable.
class SaletObj
  constructor: (spec) ->
    unless spec.name?
      console.error("Trying to create an object with no name")
      return null

    @order = 0 # you can use this to sort the descriptions
    @visible = true
    @look = (system, f) =>
      if @dsc and @dsc != "" and @visible
        text = markdown(@dsc.fcall(this, system, f).toString())
        # replace braces {{}} with link to _act_
        return parsedsc(text, @name)
    @takeable = false
    @take = (system) => "You take the #{@name}." # taking to inventory
    @act = (system) => "You don't find anything extraordinary about the #{@name}." # object action
    @dsc = (system) => "You see a {{#{@name}}} here." # object description
    @inv = (system) => "It's a #{@name}." # inventory description
    @location = ""
    @put = (salet, location) =>
      @level = 0 # this is scenery
      if salet.rooms[location]?
        @location = location
        salet.rooms[location].take(this)
      else
        console.log("Could not find location #{location} for an object #{@name}")
    @delete = (salet, location = false) =>
      if location == false
        location = @location
      salet.rooms[location].drop(this)

    for key, value of spec
      this[key] = value
  
obj = (name, spec) ->
  spec ?= {}
  spec.name = name
  return new SaletObj(spec)
module.exports = obj

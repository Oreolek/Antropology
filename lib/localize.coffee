# Internationalization support

languages = {}

# Default Messages

en = {
  terrible: "terrible",
  poor: "poor",
  mediocre: "mediocre",
  fair: "fair",
  good: "good",
  great: "great",
  superb: "superb",
  yes: "yes",
  no: "no",
  choice: "Choice {number}",
  no_group_definition: "Couldn't find a group definition for {id}.",
  link_not_valid: "The link '{link}' doesn't appear to be valid.",
  link_no_action: "A link with a situation of '.', must have an action.",
  unknown_situation: "You can't move to an unknown situation: {id}.",
  existing_situation: "You can't override situation {id} in HTML.",
  erase_message: "This will permanently delete this character and immediately return you to the start of the game. Are you sure?",
  no_current_situation: "I can't display, because we don't have a current situation.",
  no_local_storage: "No local storage available.",
  random_seed_error: "You must provide a valid random seed.",
  random_error: "Initialize the Random with a non-empty seed before use.",
  dice_string_error: "Couldn't interpret your dice string: '{string}'."
}

# Set this data as both the default fallback language, and the english preferred language.
languages[""] = en
languages["en"] = en

languageCodes = Object.keys(languages)

localize = (languageCode, message) ->
  for thisCode in languageCodes
    if languages[languageCode]?
      localized = languages[languageCode][message]
      if localized
        return localized
  return message

# API
String.prototype.l = (args) ->
  # Get lang attribute from html tag.
  lang = document.getElementsByTagName("html")[0].getAttribute("lang") || ""

  # Find the localized form.
  localized = localize(lang, this)
  if typeof(localized) == "function"
    localized = localized(args)
  else # Merge in any replacement content.
    if args
      for name in args
        localized = localized.replace(
          new RegExp("\\{"+name+"\\}"), args[name]
        )
  return localized

module.exports = languages;

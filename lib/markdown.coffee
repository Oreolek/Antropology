###
Indent normalization. Removes tabs AND spaces from every line beginning.
Implies that you don't mix up your tabs and spaces.
Copyright 2015 Bruno Dias
###
normaliseTabs = (text) ->
  unless text? and typeof(text) == "string"
    return ""
  lines = text.split('\n');
  indents = lines
    .filter((l) => l != '')
    .map((l) => l.match(/^\s+/))
    .map((m) ->
      if (m == null)
        return ''
      return m[0]
    )
  smallestIndent = indents.reduce((max, curr) ->
    if (curr.length < max.length)
      return curr
    return max
  )
  return lines.map((l) ->
    return l.replace(new RegExp('^' + smallestIndent), '')
  ).join('\n')

markdown = (text) ->
  unless text?
    return ""
  if typeof text is Function
    text = text()
  return marked(normaliseTabs(text), {
    smartypants: true
  })

module.exports = markdown

room = require("../../lib/room.coffee")
Salet = require('../../lib/salet.coffee')

# object merging utility function
merge=(xs...) ->
  if xs?.length>0
    tap {},(m)->m[k]=v for k,v of x for x in xs
tap=(o, fn)->fn(o);o

languages = require('../../lib/localize.coffee')
languages["ru"] = require('../../game/language/ru.coffee')

salet = Salet({
  game_id: "9845fbbf-685b-4b44-aa24-eb758af7c8ff"
  game_version: "1.0"
  start: "sleep"
})

$(document).ready(() ->
  salet.beginGame()
  player = new ChiptuneJsPlayer(new ChiptuneJsConfig(-1))
  player.load("audio/boringn.xm", (buffer) ->
    player.play(buffer)
  )
  player2 = new ChiptuneJsPlayer(new ChiptuneJsConfig(-1))
  player2.load("audio/chipmunk.mod", (buffer) ->
    player2.play(buffer)
    player2.togglePause()
  )
  $("#create").click(() ->
    $("body").addClass("flash")
    setTimeout(() ->
      $("body").removeClass("flash")
      player.togglePause()
      player2.togglePause()
    , 1000)
    player.togglePause()
    player2.togglePause()
    opacity = $("#create").css("opacity")
    switch
      when opacity > 0.8
        $("#create").fadeTo("fast", 0.7)
      when opacity > 0.4
        $("#create").fadeTo("fast", 0.3)
      else
        salet.character.times = 0
        $("#create").hide()
        $("#create").css("opacity", "1.0")
    return true
  )
)

actlink = (content, ref) ->
  return "<a href='./#{ref}' class='once'>#{content}</a>"

getRandom = (arr, id) ->
  retval = arr[Math.floor(Math.random()*arr.length)]
  if retval == id
    return getRandom arr,id
  return retval

randomDarkColour = () ->
  return "hsl(#{Math.floor(Math.random()*360)},#{Math.floor(Math.random()*100)}%,#{Math.floor(Math.random()*50)}%)"

salet.realworld = []
realworld = (id, salet, linktitle) ->
  salet.realworld[salet.realworld.length] = id
  options = {
    dsc: actlink(linktitle, 'next')
    before: () ->
      salet.character.times ?= 1
      salet.character.times += 1
      $("#current-room").css({
        "position": "absolute"
        "z-index": "4"
        "top": (64+Math.random()*300)+"px"
        "left": Math.random()*300+"px"
      })
    after: () ->
      if salet.character.times > 4
        $("#create").show()
      $("#current-room a").css("color", randomDarkColour())
    actions:
      next: (salet) ->
        return salet.goTo(getRandom(salet.realworld, id))
  }
  return room(id, salet, options)

realworld "sleep", salet, "SLEEP".l()
realworld "sex", salet, "SEX".l()
realworld "eat", salet, "EAT".l()
realworld "drink", salet, "DRINK".l()
realworld "dream", salet, "DREAM".l()
realworld "socialize", salet, "SOCIALIZE".l()
realworld "walk", salet, "WALK".l()
realworld "smile", salet, "SMILE".l()
realworld "frown", salet, "FROWN".l()
realworld "wakeup", salet, "WAKE UP".l()
realworld "evolve", salet, "EVOLVE".l()
realworld "degenerate", salet, "DEGENERATE".l()
